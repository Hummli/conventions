# Clone a repository
To clone a repository to your local filesystem use `git clone <http://your-repository-url>`

# Pull from repository
Get changes from server: `git pull`

# Push changes to repository
1. `git add <your files>`
2. `git commit -m "your message"`
3. `git push`

# Merge
1. `git pull`
2. `git merge`
3. `git add <your files>`
4. `git commit` -> to leave the editor use `:q + Enter`
5. `git push`

