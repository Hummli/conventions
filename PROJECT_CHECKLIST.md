<!-- markdownlint-disable MD041 -->
<!-- First line should be top level heading disabled, because this text is the issue body -->

Quality Assurance in software projects is a topic that needs to be addressed
from the beginning. Please make sure you fulfill the requirements from this
checklist when starting your Inner Source project.

* [ ] **Organizational and People topics**
  * [ ] BU/Department agrees on making this component *Inner Source*.
    As the specific processes differ please consider consulting a
    [Social Coding Ambassdor](https://gitlab.com/Hummli)
    in your organization upfront.
  * [ ] People are ready to collaborate across silos, see e.g. [team
  charter](https://gitlab.com/Hummli/conventions/TEAM_CHARTER.md)
  * [ ] 2 or more maintainers are defined
  * [ ] Funding of maintainers is available
  * [ ] Open source skills of maintainers verified
  * [ ] Involved people are familiar with git

* [ ] **Licensing**: choose the license according to the projects goals and
  context...
  * [ ] Either use the [Siemens Inner Source
    License](https://gitlab.com/Hummli/conventions/LICENSE.md)
  * [ ] Or define the licensing model together with your legal department

* [ ] **Ownership**: 


* [ ] **Documentation**: you can use
  [Markdown](https://en.wikipedia.org/wiki/Markdown) to nicely integrate your
  docs on gitlab
  * [ ] `README.md` describes the component and its usage
  * [ ] All needed documentation part of the repository
  * [ ] `CONTRIBUTING.md` available, example available in the
  [Hummli/conventions](https://gitlab.com/Hummli/conventions/CONTRIBUTING.md) repository
  * [ ] `.editorconfig`  available.
  EditorConfig helps developers define and maintain consistent
  basic coding style, such as line ending indent, tabs,
  etc. across editors, IDEs and operating systems.
  More info on [editorconfig.org](http://editorconfig.org/)

* [ ] **Workflow**
  * [ ] Do not work directly on master (Protected master branch)
  * [ ] Use the [merge request
  workflow](https://gitlab.com/Hummli/conventions/MERGE_REQUESTS.md)
  and [Conventional
  Changelog](https://gitlab.com/Hummli/conventions/conventions.png)
  * [ ] `CHANGELOG.md` available and auto generated, see
  [changelog](https://gitlab.com/Hummli/conventions/CHANGELOG.md) project for details

* [ ] **Build, Test and Deploy**
  * [ ] Component can be built using _Continuous Integration_:
  `.gitlab-ci.yml`, see [ci](https://gitlab.com/Hummli/conventions/GITLAB_CI.md)
    * 3rd party software dependencies are defined via language specific best
      practice, e.g. `package.json`, `pom.xml`, `requirements.txt`, `Gemfile`
    * Unit tests are part of the repository
    * Coding standards are enforced using CI

* [ ] **Copyright**
  * [ ] Ensure all source files contain a header with a correct copyright and a
    license reference, e.g.:

    ```txt
    Copyright (C), its2morrow Gbr 2019
    Licensed under the its2morrow License [actual version] or at your option any later version.
    ```